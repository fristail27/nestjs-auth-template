import { Column, Entity } from 'typeorm';
import {IsArray, IsBoolean, IsString, Length, IsOptional} from 'class-validator';
import {EntityBase} from "../../core/utils/entity_base";
import {loginLength, Role} from "../../core/constants";
import {IChangedPass} from "../../core/types/IChangedPass";

@Entity()
export class User extends EntityBase {
  @Column()
  @IsString()
  @Length(loginLength.min, loginLength.max)
  login: string;

  @Column()
  @IsString()
  email: string;

  @Column()
  @IsString()
  @IsOptional()
  registerIp: string;

  @Column()
  @IsString()
  @Length(loginLength.min, loginLength.max)
  name: string;

  @Column()
  @IsString()
  @Length(loginLength.min, loginLength.max)
  surname: string;

  @Column()
  @IsString()
  @IsOptional()
  passwordHash: string;

  @Column({ default: false })
  @IsBoolean()
  accountIsConfirm: boolean;

  @Column({ type: 'text', array: true, default: [Role.user] })
  @IsArray()
  role: Role[];

  @Column({ type: 'text', array: true, default: [] })
  @IsArray()
  changedPass: IChangedPass[];
}
