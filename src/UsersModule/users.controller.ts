import {
    Controller,
    Get,
    UseGuards,
    Param,
    Delete, Put, UseInterceptors, UploadedFile, Req, Res,
} from '@nestjs/common';
import {FileInterceptor} from "@nestjs/platform-express";
import {Request, Response} from "express";
import {UsersService} from "./users.service";
import {AuthGuard} from "../core/guards/auth.guard";
import {AuthService} from "../AuthModule/auth.service";

@Controller('api')
@UseGuards(AuthGuard)
export class UsersController {
    constructor(
        private readonly usersService: UsersService,
        private readonly authService: AuthService,
    ) {
    }

    @Get('user/:userId')
    async getUserById(@Param('userId') id: number) {
        return await this.usersService.findUserById(id);
    }

    @Get('allUsers')
    async getAllUsers() {
        return await this.usersService.getAllUsers();
    }

    @Put('user/update-avatar')
    @UseInterceptors(FileInterceptor('file'))
    async updateAvatar(
        @UploadedFile() file: Express.Multer.File,
        @Req() request: Request,
        @Res() response: Response,
    ) {
        const {userId} = this.authService.checkToken(request.headers.authorization as string)

        return this.usersService.uploadAvatar(userId, response, file)
    }

    @Delete('user/:userId')
    async removeUser(@Param('userId') id: number) {
        return await this.usersService.removeUser(id);
    }
}
