import { NestFactory } from '@nestjs/core';
import { AppModule } from './AppModule/app.module';
import { ConfigService } from '@nestjs/config';
import {TypeOrmUniqueViolationInterceptor} from "./core/utils/typeorm-unique-violation.interceptor";
import * as cookieParser from 'cookie-parser';


async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const configService = app.get<any, any>(ConfigService);
  // app.useGlobalInterceptors(new TypeOrmUniqueViolationInterceptor());
  app.enableCors({
    origin: [`${configService.get('clientUrl')}`],
    credentials: true,
  });
  app.use(cookieParser());

  await app.listen(configService.get('port'));
}
bootstrap();
