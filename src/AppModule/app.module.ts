import {Module} from '@nestjs/common';
import {ConfigModule, ConfigService} from "@nestjs/config";
import configuration from "../../configuration";
import {AuthModule} from "../AuthModule/auth.module";
import {TypeOrmModule} from "@nestjs/typeorm";
import {MailerModule} from "@nestjs-modules/mailer";
import {User} from "../UsersModule/entities/user.entity";
import {UsersModule} from "../UsersModule/users.module";
import {MailModule} from "../MailModule/mail.module";
import {ResetPasswordEntity} from "../AuthModule/entities/resetPassword.entity";
import {AvatarModule} from "../AvatarModule/avatar.module";

@Module({
    imports: [
        AuthModule,
        UsersModule,
        MailModule,
        AvatarModule,
        MailerModule.forRootAsync({
            imports: [ConfigModule],
            useFactory: (config: ConfigService) => ({
                ...config.get('mailData'),
            }),
            inject: [ConfigService],
        }),
        TypeOrmModule.forRootAsync({
            imports: [ConfigModule],
            useFactory: (config: ConfigService) => ({
                ...config.get('database'),
                entities: [User, ResetPasswordEntity],
                synchronize: true,
            }),
            inject: [ConfigService],
        }),
        ConfigModule.forRoot({
            load: [configuration],
            envFilePath: ['.env.development'],
        }),],
    controllers: [],
    providers: [],
})
export class AppModule {
}
