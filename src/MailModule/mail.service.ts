import { Injectable } from '@nestjs/common';
import {ISendMailOptions, MailerService} from '@nestjs-modules/mailer';
import {ConfigService} from "@nestjs/config";
import {getSuccessRegistration} from "../core/mails/getSuccessRegistration";
import {getConfirmAccount} from "../core/mails/getConfirmAccount";
import {JwtService} from "@nestjs/jwt";
import {User} from "../UsersModule/entities/user.entity";
import {getResetPassword} from "../core/mails/getResetPassword";
import {getResetPasswordSuccess} from "../core/mails/getResetPasswordSuccess";

@Injectable()
export class MailService {
  constructor(
    private readonly mailerService: MailerService,
    private readonly configService: ConfigService,
    private readonly jwtService: JwtService,
  ) {}
  private sendMail(options: ISendMailOptions) {
    return this.mailerService.sendMail(options)
  }
  async sendRegistrationSuccess (user: User) {
    const getMailData = this.configService.get('getMailServiceData');

    const mailOptions = getMailData(
        user.email,
        'Вы успешно зарегестрировались',
        getSuccessRegistration(user.email, user.login, user.name, user.surname),
    );
    await this.sendMail(mailOptions);

  }

  async sendConfirmAccount (user: User) {
    const getMailData = this.configService.get('getMailServiceData');

    const confirmJWT = this.jwtService.sign({
      userId: user.id,
    }, {
      secret: process.env.JWT_SECRET_KEY,
      privateKey: process.env.JWT_PRIVATE_KEY,
      expiresIn: 1000000000
    })
    const confirmMailOptions = getMailData(
        user.email,
        'Подтверждение аккаунта',
        getConfirmAccount(user.login, `${process.env.APP_URL}/confirm-account?key=${confirmJWT}`),
    );
    await this.sendMail(confirmMailOptions)
  }

  async sendResetPassword (user: User, id: string) {
    const getMailData = this.configService.get('getMailServiceData');
    const mailOptions = getMailData(
        user.email,
        'Восстановление пароля',
        getResetPassword(
            user.email as string,
            user.login as string,
            `${process.env.APP_URL}/confirm-reset-password?key=${id}`
        ),
    );
    await this.sendMail(mailOptions)
  }

  async sendUpdatePasswordSuccess (email: string) {
    const getMailData = this.configService.get('getMailServiceData');
    const mailOptions = getMailData(
        email,
        'Пароль успешно изменен',
        getResetPasswordSuccess(),
    );
    await this.sendMail(mailOptions)
  }
}
