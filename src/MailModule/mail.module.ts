import {Module} from '@nestjs/common';
import {MailService} from "./mail.service";
import {ConfigService} from "@nestjs/config";
import {JwtService} from "@nestjs/jwt";

@Module({
    providers: [JwtService, ConfigService, MailService],
    exports: [MailService],
})
export class MailModule {}
