import {forwardRef, Module} from '@nestjs/common';
import {TypeOrmModule} from "@nestjs/typeorm";
import {EntityClassOrSchema} from "@nestjs/typeorm/dist/interfaces/entity-class-or-schema.type";
import {AuthService} from './auth.service';
import {AuthController} from './auth.controller';
import {JwtModule} from "@nestjs/jwt";
import {ConfigModule, ConfigService} from "@nestjs/config";
import {User} from "../UsersModule/entities/user.entity";
import {UsersModule} from "../UsersModule/users.module";
import {MailModule} from "../MailModule/mail.module";
import {ResetPasswordEntity} from "./entities/resetPassword.entity";

@Module({
    providers: [AuthService, JwtModule],
    imports: [
        forwardRef(() => UsersModule),
        forwardRef(() => MailModule),
        TypeOrmModule.forFeature([User, ResetPasswordEntity] as EntityClassOrSchema[]),
        JwtModule.registerAsync({
            imports: [ConfigModule],
            useFactory: async (config: ConfigService) => ({
                secret: config.get('secretKey'),
                signOptions: { expiresIn: config.get('expiresIn') },
            }),
            inject: [ConfigService],
        }),
    ],
    controllers: [AuthController],
    exports: [AuthService],
})
export class AuthModule {
}
