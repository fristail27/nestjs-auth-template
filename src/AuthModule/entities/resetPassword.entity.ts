import {Column, CreateDateColumn, Entity, PrimaryColumn, UpdateDateColumn} from 'typeorm';
import {IsString, IsNotEmpty, IsDate, IsNumber, IsPositive} from 'class-validator';

@Entity()
export class ResetPasswordEntity {
  @PrimaryColumn()
  @IsString()
  @IsNotEmpty()
  id: string;

  @Column()
  @IsNumber()
  @IsPositive()
  @IsNotEmpty()
  userId: number;

  @CreateDateColumn()
  @IsDate()
  createdAt: Date;
  @UpdateDateColumn()
  @IsDate()
  updateAt: Date;
}
