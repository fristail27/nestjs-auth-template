import {BadRequestException, Injectable} from '@nestjs/common';
import {InjectRepository} from "@nestjs/typeorm";
import {JwtService} from "@nestjs/jwt";
import {EntityClassOrSchema} from "@nestjs/typeorm/dist/interfaces/entity-class-or-schema.type";
import {Repository} from "typeorm";
import {v4} from "uuid";
import {UsersService} from "../UsersModule/users.service";
import {User} from "../UsersModule/entities/user.entity";
import {CreateUserDto} from "../UsersModule/dto/createUser.dto";
import {MailService} from "../MailModule/mail.service";
import {ResetPasswordEntity} from "./entities/resetPassword.entity";
import {ResetPasswordDto} from "./dto/resetPassword.dto";
import {UpdatePassDto} from "./dto/updatePass.dto";

@Injectable()
export class AuthService {
    constructor(
        @InjectRepository(User as EntityClassOrSchema)
        private readonly usersRepository: Repository<User>,
        @InjectRepository(ResetPasswordEntity as EntityClassOrSchema)
        private readonly resetPasswordRepository: Repository<ResetPasswordEntity>,
        private readonly usersService: UsersService,
        private readonly mailService: MailService,
        private readonly jwtService: JwtService,
    ) {}

    async signup(createUserDto: CreateUserDto, ip: string) {
        const userByEmail = await this.usersService.findUserByEmail(createUserDto.email)
        if (userByEmail) {
            throw new BadRequestException('Пользователь с таким E-mail уже существует');
        }
        const userByLogin = await this.usersService.findUserByLogin(createUserDto.login)
        if (userByLogin) {
            throw new BadRequestException('Пользователь с таким ником уже существует');
        }
        const newUser = await this.usersService.create({...createUserDto, registerIp: ip} as User);

        await this.mailService.sendRegistrationSuccess(newUser);
        await this.mailService.sendConfirmAccount(newUser);
        const accessToken = this.getAccessToken(newUser.id)
        const refreshToken = this.getRefreshToken(newUser.id)
        return {
            user: {
                id: newUser.id,
                login: newUser.login,
                email: newUser.email,
                name: newUser.name,
                surname: newUser.surname,
                accountIsConfirm: newUser.accountIsConfirm,
                role: newUser.role,
            }, accessToken, refreshToken
        };
    }

    async confirmAccount (key: string) {
        try {
            const decodeKey = this.checkToken(key)
            const userInfo = await this.usersService.findUserById(decodeKey.userId)
            const res = await this.usersService.update({...userInfo, accountIsConfirm: true} as User)
            const accessToken = this.getAccessToken(res.id)
            const refreshToken = this.getRefreshToken(res.id)
            return {userInfo: res, accessToken, refreshToken}
        } catch (err) {
            throw new BadRequestException('Неправильный токен');
        }
    }

    checkToken (token:string): { userId: number, iat: number, exp: number } {
        return this.jwtService.verify(token, {
            secret: process.env.JWT_SECRET_KEY,
        })
    }

    getAccessToken (userId: number) {
        return this.jwtService.sign({
            userId: userId,
        }, {
            secret: process.env.JWT_SECRET_KEY,
            privateKey: process.env.JWT_PRIVATE_KEY,
            expiresIn: 10 * 60,
        })
    }

    getRefreshToken (userId: number) {
        return this.jwtService.sign({
            userId: userId,
        }, {
            secret: process.env.JWT_SECRET_KEY,
            privateKey: process.env.JWT_PRIVATE_KEY,
            expiresIn: 60 * 60 * 24 * 100,
        })
    }

    async signInByRefreshToken (accessToken: string) {
        try {
            const {userId} = this.checkToken(accessToken)
            const userData = await this.usersService.findUserById(userId)
            const newAccessToken = this.getAccessToken(userId)
            return {
                user: {
                    id: userData.id,
                    login: userData.login,
                    email: userData.email,
                    name: userData.name,
                    surname: userData.surname,
                    accountIsConfirm: userData.accountIsConfirm,
                    role: userData.role,
                }, accessToken: newAccessToken
            }
        } catch (err) {
            return null
        }
    }

    async signInByEmailOrLogin (login: string, passwordHash: string) {
        const isEmail = login.includes('@')
        let user
        if (isEmail) {
            user = await this.usersService.findUserByEmail(login)
        } else {
            user = await this.usersService.findUserByLogin(login)
        }
        if (!user || user.passwordHash !== passwordHash) return null

        const accessToken = this.getAccessToken(user.id)
        const refreshToken = this.getRefreshToken(user.id)

        return {
            user: {
                id: user.id,
                login: user.login,
                email: user.email,
                name: user.name,
                surname: user.surname,
                accountIsConfirm: user.accountIsConfirm,
                role: user.role,
            }, accessToken, refreshToken
        }
    }

    async resetPassword (login:string) {
        const isEmail = login.includes('@')
        let user
        if (isEmail) {
            user = await this.usersService.findUserByEmail(login)
        } else {
            user = await this.usersService.findUserByLogin(login)
        }
        if (!user) return null
        const newId = v4()
        const newObj: ResetPasswordDto = {id: newId, userId: user.id}
        await this.resetPasswordRepository.save(newObj)

        await this.mailService.sendResetPassword(user, newId)
        return true
    }

    async checkLinkResetPassword (id: string) {
        const resetPassInst = await this.resetPasswordRepository.findOne({
            where: {id},
        })
        if (resetPassInst) {
            return true
        } else {
            throw new BadRequestException('Ссылка недействительна');
        }
    }

    async updatePassword ({passwordHash, key}: UpdatePassDto, ip: string) {
        const resetPassInst = await this.resetPasswordRepository.findOne({
            where: {id: key},
        })

        if (resetPassInst) {
            const currentUser = await this.usersService.findUserById(resetPassInst.userId)
            if (currentUser.passwordHash === passwordHash) {
                throw new BadRequestException('Новый пароль не может быть идентичен старому');
            }
            await this.usersRepository.save({
                id: resetPassInst.userId,
                passwordHash,
                changedPass:  [...currentUser.changedPass, {
                    time: new Date().toISOString(), ip, oldPassHash: currentUser.passwordHash
                }]
            })

            await this.mailService.sendUpdatePasswordSuccess(currentUser.email)
            await this.resetPasswordRepository.remove(resetPassInst)
            return true
        } else {
            throw new BadRequestException('Ссылка недействительна');
        }
    }
}
