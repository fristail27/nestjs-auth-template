export const SAVE_FILES_PATH = 'files';

export enum PostgresErrorCode {
    UniqueViolation = '23505',
    CheckViolation = '23514',
    NotNullViolation = '23502',
    ForeignKeyViolation = '23503',
}

export enum Role {
    user = 'user',
    admin = 'admin',
}

export const enum loginLength {
    min = 1,
    max = 20,
}

export type MailServiceData = {
    from: { name: string; address: string };
    subject: string;
    html: string;
    to: string;
}
