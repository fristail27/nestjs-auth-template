export const getResetPassword = (
    email: string,
    nickname: string,
    resetUrl: string
) => `<section>
        <h1>Восстановление паролья</h1>
        <p>Регистрационные данные вашего аккаунта:</p>
        <p>Email: ${email}</p>
        <p>Логин: ${nickname}</p>
        <p>Для изменения пароля перейдите <a href=${resetUrl}>по ссылке</a></p>
</section>`;
