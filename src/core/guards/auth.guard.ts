import { Injectable, CanActivate, ExecutionContext } from '@nestjs/common';
import {Request} from "express";
import { Observable } from 'rxjs';
import {JwtService} from "@nestjs/jwt";

const validateRequest = (req: Request, jwtService: JwtService): boolean => {
    const accessToken = req.headers.authorization
    if (!accessToken) return false
    try {
        jwtService.verify(accessToken, {
            secret: process.env.JWT_SECRET_KEY,
        })
        return true
    } catch (err) {
        return false
    }
}

@Injectable()
export class AuthGuard implements CanActivate {
    constructor(
        private readonly jwtService: JwtService,
    ) {}
    canActivate(
        context: ExecutionContext,
    ): boolean | Promise<boolean> | Observable<boolean> {
        const request = context.switchToHttp().getRequest();
        return validateRequest(request, this.jwtService);
    }
}