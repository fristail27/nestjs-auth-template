import {
  CallHandler,
  ConflictException,
  ExecutionContext,
  Injectable,
  NestInterceptor,
} from '@nestjs/common';
import { catchError, Observable } from 'rxjs';
import { QueryFailedError } from 'typeorm';
import { PostgresErrorCode } from '../constants';

@Injectable()
export class TypeOrmUniqueViolationInterceptor implements NestInterceptor {
  intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
    return next.handle().pipe(
      catchError((error: unknown) => {
        // Если это не TypeORM ошибка выполнения запроса, то пробрасываем ее дальше
        if (!(error instanceof QueryFailedError)) {
          throw error;
        }

        // Вытаскиваем из ошибки полезные данные
        const {
          table = 'unknown',
          code = -1,
          detail = '',
        } = error.driverError || {};

        // Если это не ошибка уникальности, то пробрасываем ее дальше
        if (code !== PostgresErrorCode.UniqueViolation) {
          throw error;
        }

        // Не падаем и возвращаем клиенту ошибку 409
        throw new ConflictException(
          `Значение в таблице ${table} уже существует`,
          detail,
        );
      }),
    );
  }
}
