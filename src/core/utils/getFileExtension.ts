export const getFileExtension = (fileName:string) => {
    const fileArr = fileName.split('.')
    const name = fileArr.filter((c, i) => i < fileArr.length - 1 ).join('.')
    return {
        extension: fileArr[fileArr.length - 1].toLowerCase(),
        fileName: name
    }
}