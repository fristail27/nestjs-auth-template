import {MailServiceData} from "./src/core/constants";

export default () => ({
  apiUrl: process.env.API_URL,
  clientUrl: process.env.CLIENT_URL,
  port: parseInt((process.env.APP_PORT || '3000'), 10),
  database: {
    type: process.env.DB_TYPE,
    host: process.env.DB_HOST,
    port: parseInt(process.env.DB_PORT as string, 10),
    username: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_NAME,
  },
  secretKey: process.env.SECRET_KEY,
  refreshSecretKey: process.env.REFRESH_SECRET_KEY,
  expiresInAccessToken: 900,
  expiresInRefreshToken: '60d',
  salt: 10,
  passwordResetExpiration: 600000,
  mailData: {
    transport: {
      service: 'Yandex',
      auth: {
        user: process.env.MAIL_USER,
        pass: process.env.MAIL_PASSWORD,
      },
    },
  },
  getMailServiceData: (
      to: string,
      subject: string,
      template: string,
  ): MailServiceData => ({
    from: { name: 'Видеохостинг', address: process.env.MAIL_USER as string },
    subject,
    html: template,
    to,
  }),
});